import module.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(01, "Thai", "Teo", 1000);

        Employee employee2 = new Employee(01, "Thai", "Beo", 900);
        System.out.println("Employee1");
        System.out.println("Luong mot nam la: " + employee1.getAnnualSalary());
        System.out.println("Luong tang 5%: " + employee1.raiseSalary(5));

        System.out.println("Employee2");
        System.out.println("Luong mot nam la: " + employee2.getAnnualSalary());
        System.out.println("Luong tang 12%: " + employee2.raiseSalary(12));

    }
}
