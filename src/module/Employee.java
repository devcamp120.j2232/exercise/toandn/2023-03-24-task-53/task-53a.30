package module;

public class Employee {
  private int id;
  private String firstName;
  private String lastName;
  private int salary;
  
  public Employee(int id, String firstName, String lastName, int salary) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.salary = salary;
  }

  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getName(){
    return this.firstName + " " + this.lastName;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  public int getAnnualSalary(){
    return this.salary*12;
  }

  public double raiseSalary(double percent){
    return Math.round((1 + percent/100)*this.salary*100)/100.0;
  }

  public String toString(){
    return "Emloyee[id=" + this.id + ",name=" + this.firstName + " " + this.lastName + ", salary=" + this.salary + "]";
  }
}
